%if 0%{?fedora} >= 36 || 0%{?rhel} > 9
%global dict_dirname hunspell
%else
%global dict_dirname myspell
%endif

Name: hunspell-bm
Summary: Bambara hunspell dictionaries
%global upstreamid 2.5
Version: %{upstreamid}
Release: 4%{?dist}
Source0: https://addons.mozilla.org/firefox/downloads/file/3535932/bambarabamanakan-2.5.xpi
Source1: https://gitlab.com/ci-dict/hunspell-bm/-/archive/2.5/hunspell-bm-2.5.tar.gz

URL: https://extensions.libreoffice.org/en/extensions/show/bambara-bamanakan-spell-checker
License: BSD-3
BuildArch: noarch

BuildRequires: unzip
Requires: hunspell-filesystem
Supplements: (hunspell)

%description
Bambara hunspell dictionaries.

%prep
#%autosetup -c -n hunspell-bm
%autosetup -T -D -a 1 -c -n hunspell-bm
#%setup -n hunspell-bm
#%setup -T -D -a 1 -n hunspell-bm

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}
cp -p dictionaries/bm-ML.dic $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}/bm_ML.dic
cp -p dictionaries/bm-ML.aff $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}/bm_ML.aff

%files
%doc hunspell-bm-2.5/README.adoc
%license hunspell-bm-2.5/LICENSE.txt
%{_datadir}/%{dict_dirname}/*

%changelog
* Thu Jul 14 2022 Boyd Kelly <bkelly@fedoraproject.org> - 025
- hunspell for Bambara - Change - to _ in dest dic and aff; version 25

* Wed Jul 13 2022 Boyd Kelly <bkelly@fedoraproject.org> - 025
- hunspell for Bambara - Change source0 to mozilla extension; version 25
  Add conditional for hunspell dir path and requires update:
  hunspell-filesystem

* Wed Apr 14 2021 Boyd Kelly <bkelly@fedoraproject.org> - 024
- hunspell for Bambara - initial build 024

